"""
Firebase configuration
"""
import os


class BaseConfig:
    """Base class for configuration
    """

    def __init__(self, file, cfg_psr):
        self.file = file
        self.parser = cfg_psr

    def _check_file(self):
        if not os.path.exists(self.file):
            raise FileNotFoundError(f"Config file not found, file: {self.file}")

    def read(self):
        self._check_file()
        # read file
        self.parser.read(self.file)

        return self

    def value(self, section, option):
        # get a value for a specific section and option
        return self.parser.get(section, option)


class FirebaseConfig:
    """Config class for firebase
    """

    def __init__(self, cfg):
        self.cfg = cfg
        self.section = "firebase"

    @property
    def database_url(self):
        return self.cfg.value(self.section, "database_url")


class EntsoeConfig:
    """Config class for entsoe
    """

    def __init__(self, cfg):
        self.cfg = cfg
        self.section = "entsoe"

    @property
    def endpoint(self):
        return self.cfg.value(self.section, "endpoint")

    @property
    def resources(self):
        return [res.strip() for res in self.cfg.value(self.section, "resources").split(",")]

    @property
    def params(self):
        return {
            "securityToken": self.cfg.value(self.section, "securityToken"),
            "documentType" : self.cfg.value(self.section, "documentType"),
            "processType"  : self.cfg.value(self.section, "processType"),
            "in_Domain"    : self.cfg.value(self.section, "in_Domain"),
            "psrType"      : self.cfg.value(self.section, "psrType"),
            "periodStart"  : self.cfg.value(self.section, "periodStart"),
            "periodEnd"    : self.cfg.value(self.section, "periodEnd")
            }

    def __iter__(self):
        return iter(self.resources)

    def __getitem__(self, item):
        return self.resources[item]
