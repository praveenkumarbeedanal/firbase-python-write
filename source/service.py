"""
Firebase services

"""

import json
from bs4 import BeautifulSoup

import requests


class Firebase:
    """class that provides firebase services
    """

    def __init__(self, fb_cfg):
        self.database_url = fb_cfg.database_url

    def __str__(self):
        return "Connection to firebase services"

    def realtime_database(self):
        return RealtimeDatabase(self.database_url)


class RealtimeDatabase:
    """class the perform REST API calls to Realtime database
    """

    def __init__(self, database_url):
        self.database_url = database_url
        self.path = ""

    def _check_token(self):
        return f"{self.database_url}{self.path}.json"

    def post(self, path, data):
        # TODO handle response errors?
        self.path = path
        response = requests.post(self._check_token(), data=data)
        return response.json()


PSR_TYPES = {
    'B01': ['Biomass', 18],
    'B02': ['Fossil Brown coal', 1001],
    'B03': ['Fossil Coal', 1001],
    'B04': ['Fossil Gas', 469],
    'B05': ['Fossil Hard coal', 1001],
    }


class Entsoe:

    def __init__(self, entsoe_cfg):
        self.cfg = entsoe_cfg

        self.endpoint = self.cfg.endpoint
        self.params = self.cfg.params

        self.data = {}
        self.sum_ef = 0
        self.sum_quantity = 0

        self.co2 = 0

    def setup(self):
        # loop over all resources
        for res in self.cfg:
            # set url psrType
            self.params["psrType"] = res

            # TODO handle unexpected type
            quantity = float(BeautifulSoup(requests.get(self.endpoint, params=self.params).text,
                                           "html.parser").find("quantity").text)

            res_ef = quantity * 0.25 * PSR_TYPES.get(res)[-1]

            self.sum_quantity += quantity
            self.sum_ef += res_ef
            self.data[res] = quantity

    def resources(self):
        res_data = {PSR_TYPES.get(key)[0]: val for key, val in self.data.items()}
        res_data.update({"periodStart": self.params.get("periodStart")})
        # default UTF-8
        return json.dumps(res_data).encode()

    def calculate_co2(self):
        return self.sum_ef / self.sum_quantity

    def emission_factor(self):
        ef_data = {
            "co2"        : self.calculate_co2(),
            "periodStart": self.params.get("periodStart")
            }
        return json.dumps(ef_data).encode()
